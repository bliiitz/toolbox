FROM python:3.6-buster

ENV TERRAFORM_VERSION="0.12.18"
ENV KUBECTL_VERSION="1.16.0"

# install ansible
RUN apt update -y && apt install -y software-properties-common 
RUN apt install -y  git gcc curl unzip apt-transport-https jq gnupg2 ca-certificates
RUN pip install ansible awscli python-gitlab pyyaml requests kubernetes

RUN add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable" && \
   curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - && \
   apt update && apt install docker-ce -y


RUN export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)" && \
    echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && \
    curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - && \
    apt-get update -y && apt-get install google-cloud-sdk -y

RUN curl -L -o terraform.zip "https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip" && \
unzip terraform.zip -d /usr/local/bin && \
chmod +x /usr/local/bin/terraform

RUN curl -L -o /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl && \
chmod +x /usr/local/bin/kubectl

RUN curl -sSL -o /usr/local/bin/argocd https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64 && \
chmod +x /usr/local/bin/argocd

RUN curl -L -o /usr/local/bin/aws-iam-authenticator https://amazon-eks.s3-us-west-2.amazonaws.com/1.13.8/2019-08-14/bin/linux/amd64/aws-iam-authenticator && \
chmod +x /usr/local/bin/aws-iam-authenticator

RUN curl -sL https://run.linkerd.io/install | sh
RUN export PATH=$PATH:$HOME/.linkerd2/bin

RUN curl -L -o ./helm-v2.14.3-linux-amd64.tar.gz https://get.helm.sh/helm-v2.14.3-linux-amd64.tar.gz \
    && tar -zxvf helm-v2.14.3-linux-amd64.tar.gz \
    && mv linux-amd64/helm /usr/local/bin/helm
